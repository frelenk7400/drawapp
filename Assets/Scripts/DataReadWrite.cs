using System;
using System.IO;
using Newtonsoft.Json;
using UnityEngine;
public class DataReadWrite
{
  private readonly string CharacterDataInfoPath = $"{Application.persistentDataPath}/CharacterDataInfo.json";

  public void WriteCharacterData (CharacterDataInfo characterDataInfo)
  {
    string json = JsonConvert.SerializeObject(characterDataInfo);
    File.WriteAllText(CharacterDataInfoPath, json);
  }

  public CharacterDataInfo ReadCharacterData()
  {
    string json = File.ReadAllText(CharacterDataInfoPath);
    CharacterDataInfo characterDataInfo = JsonConvert.DeserializeObject<CharacterDataInfo>(json);
    return characterDataInfo;
  }

  [Serializable]
  public class CharacterDataInfo
  {
    public int ColorIndex;
    public string CurrentCharacter;

    public CharacterDataInfo (int colorIndex, string currentCharacter)
    {
      ColorIndex = colorIndex;
      CurrentCharacter = currentCharacter;
    }
  }
}