using UnityEngine;
public class CameraResizer : MonoBehaviour
{
  [SerializeField]
  private Camera _camera;
  [SerializeField]
  private float _tabletCameraSize;
  [SerializeField]
  private float _phoneCameraSize;

  private void Awake()
  {
    bool isTablet = DeviceTypeChecker.IsTablet();
    _camera.orthographicSize = isTablet ? _tabletCameraSize : _phoneCameraSize;
  }
}