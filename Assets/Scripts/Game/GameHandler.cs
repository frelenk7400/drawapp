using System;
using DefaultNamespace.Game;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
public class GameHandler : MonoBehaviour
{
  [SerializeField]
  private MainSceneSettings _sceneSettings;
  [SerializeField]
  private CalculateLineFill _calculateLineFill;
  [SerializeField]
  private Announcer _announcer;
  [SerializeField]
  private Reminder _reminder;
  [SerializeField]
  private Button _menuButton;
 

  private Shape _shape;

  private DataReadWrite _dataReadWrite;
  private DataReadWrite.CharacterDataInfo _characterDataInfo;

  private void Start()
  {
    _dataReadWrite = new();

    _menuButton.onClick.AddListener(() => LoadScene("Menu"));
    
    _characterDataInfo = _dataReadWrite.ReadCharacterData();
    
    _shape = Instantiate(_sceneSettings.HorizontalScrollElement[_characterDataInfo.CurrentCharacter].ShapePrefab);
    _shape.SetColor(_sceneSettings.HorizontalScrollElement[_characterDataInfo.CurrentCharacter].Colors[_characterDataInfo.ColorIndex]);
    _announcer.Init(_sceneSettings.HorizontalScrollElement[_characterDataInfo.CurrentCharacter].ShapeType);
    _announcer.OnAnnouncementEnd += OnAnnouncementEnd;
    _announcer.OnCheerEnd += LoadNextLevel;
  }

  private void OnAnnouncementEnd()
  {
    _reminder.Init(_shape);
    _shape.ShowPath();
    Transform transformFollowLine = Instantiate(_sceneSettings.FollowLinePathObject).transform;
    _calculateLineFill.Init(_shape, transformFollowLine);

    _shape.OnShapeClose += OnShapeClose;
  }

  private void LoadNextLevel()
  {
    int colorsInSettings = _sceneSettings.HorizontalScrollElement[_characterDataInfo.CurrentCharacter].Colors.Count;
    int nextColor = _characterDataInfo.ColorIndex + 1 >= colorsInSettings ? 0 : _characterDataInfo.ColorIndex + 1;

    _characterDataInfo.ColorIndex = nextColor;
    _dataReadWrite.WriteCharacterData(_characterDataInfo);

    LoadScene("Game");
  }

  private void LoadScene(string sceneName)
  {
    SceneManager.LoadScene(sceneName);
  }

  private void OnShapeClose()
  {
    _announcer.PlayCheers();
  }
  private void OnDestroy()
  {
    _announcer.OnAnnouncementEnd -= OnAnnouncementEnd;
    _announcer.OnCheerEnd -= LoadNextLevel;
    _shape.OnShapeClose -= OnShapeClose;
  }

 
}