using System;
using System.Collections.Generic;
using System.Linq;
using DG.Tweening;
using UnityEngine;
public class Shape : MonoBehaviour
{
  private static readonly int fillHash = Shader.PropertyToID("_fill");
  private static readonly int colorHash = Shader.PropertyToID("_Color");

  public event Action OnLineClose;
  public event Action OnShapeClose;
  
  [SerializeField]
  private SpriteRenderer _spriteRenderer;
  [SerializeField]
  private List<DrawStepInfo> _drawSteps;
  [SerializeField]
  private List<ShowStepInfo> _showSteps;

  private int _currentStep;
  private float _currentLineFill = 1;
  private bool _shapeClosed;
  private Color _lineRendererColor;

  public List<Vector3> GetLineRendererPoints()
  {
    Vector3 [] allPoints = new Vector3 [_drawSteps[_currentStep].LineRenderer.positionCount];
    _drawSteps[_currentStep].LineRenderer.GetPositions(allPoints);
    return allPoints.ToList();
  }
  public Transform LineRendererTransform => _drawSteps[_currentStep].LineRenderer.transform;
  public TraceType TraceType => _drawSteps[_currentStep].TraceType;
  public float OffsetZRotation => _drawSteps[_currentStep].offsetZRotation;
  
  public Vector3 GetShapeCenter()
  {
    if (_drawSteps[_currentStep].DrawStepCenter != null)
    {
      return _drawSteps[_currentStep].DrawStepCenter.position;
    }
    
    return _drawSteps[_currentStep].LineRenderer.transform.position;
  }
  
  public void SetColor (Color color)
  {
    _lineRendererColor = color;

    foreach (var drawStep in _drawSteps)
    {
      drawStep.LineRenderer.materials[0].SetColor(colorHash, color);
    }
  }
  public void ShowPath()
  {
    _showSteps[0].SetVisible();
  }
  
  public void SetFillValue (float lineFill)
  {
    if (!_shapeClosed)
    {
      _currentLineFill = Mathf.Clamp(lineFill, 0, 1);
      
      if (_currentLineFill <= .05f)
      {
        NextStep();
      }

      if (_currentStep < _showSteps.Count && !_shapeClosed)
      {
        _drawSteps[_currentStep].LineRenderer.materials[0].SetFloat(fillHash, _currentLineFill);
      }
    }
  }

  public List<Vector3> GetLineRendererStartEndPositions()
  {
    LineRenderer lineRenderer = _drawSteps[_currentStep].LineRenderer;

    List<Vector3> points = new List<Vector3>(new []
    {
      lineRenderer.GetPosition(0),
      lineRenderer.GetPosition(_drawSteps[_currentStep].LineRenderer.positionCount - 1)
    });

    return points;
  }

  private void NextStep()
  {
    _drawSteps[_currentStep].LineRenderer.materials[0].SetFloat(fillHash, 0);
    _currentLineFill = 1;
    
    _showSteps[_currentStep].SetActive(false);
    _currentStep++;
    

    if (_currentStep < _showSteps.Count && !_shapeClosed)
    {
      OnLineClose?.Invoke();
      _showSteps[_currentStep].SetActive(true);
      _drawSteps[_currentStep].LineRenderer.gameObject.SetActive(true);
      
      _showSteps[_currentStep].SetVisible();
    }
    else if (!_shapeClosed && _currentStep==_showSteps.Count)
    {
      OnShapeClose?.Invoke();
      _currentStep--;
      _shapeClosed = true;
    }
  }
  
  [Serializable]
  private class ShowStepInfo
  {
    public LineRenderer StepLineRenderer;
    public List<SpriteRenderer> Stars;

    public void SetActive (bool active)
    {
      StepLineRenderer.gameObject.SetActive(active);

      foreach (var star in Stars)
      {
        star.gameObject.SetActive(active);
      }
    }

    public void SetVisible()
    {
      Color currentColor = Color.white;
      currentColor.a = 0;
      Color endColor = Color.white;
      endColor.a = .9f;
      
      currentColor.a = 0;
      DOTween.To(() => currentColor, x => currentColor = x,endColor , 1)
        .OnUpdate(() =>
        {
          Material material = StepLineRenderer.materials[0];
          material.color = currentColor;

          foreach (var star in Stars)
          {
            star.color = currentColor;
          }
        });
    }
  }

  [Serializable]
  private class DrawStepInfo
  {
    public LineRenderer LineRenderer;
    public Transform DrawStepCenter;
    public TraceType TraceType;
    public float offsetZRotation;
  }
}

public enum TraceType
{
  Horizontal,
  Vertical,
  Radial
}