using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AYellowpaper.SerializedCollections;
using UnityEngine;
using Random = UnityEngine.Random;
public class Announcer : MonoBehaviour
{
  public event Action OnAnnouncementEnd;
  public event Action OnCheerEnd;
  
  [SerializeField]
  private List<AudioClip> _cheers;
  [SerializeField, SerializedDictionary("Character", "Description")]
  private SerializedDictionary<ShapeType, AudioClip> _shapeAnnouncement;
  [SerializeField]
  private AudioSource _audioSource;

  private ShapeType _shapeType;
  public void Init (ShapeType shapeType)
  {
    _shapeType = shapeType;
    PlayAnnouncement();
    float audioClipLength = _shapeAnnouncement[shapeType].length;
    FireActionWithDelay(audioClipLength, () => OnAnnouncementEnd?.Invoke());
  }

  public void PlayAnnouncement()
  {
    _audioSource.clip = _shapeAnnouncement[_shapeType];
    _audioSource.Play();
  }
  public void PlayCheers()
  {
    int randomIndex = Random.Range(0, _cheers.Count);
    _audioSource.clip = _cheers[randomIndex];
    _audioSource.Play();
    
    float audioClipLength = _cheers[randomIndex].length;
    FireActionWithDelay(audioClipLength, () => OnCheerEnd?.Invoke());
  }

  private async void FireActionWithDelay(float length, Action actionToFire)
  {
    await Task.Delay((int)(length * 1000));
    actionToFire?.Invoke();
  }
}