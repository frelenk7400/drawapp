using System.Collections.Generic;
using UnityEngine;
public class Reminder : MonoBehaviour
{
  [SerializeField]
  private Announcer _announcer;
  [SerializeField]
  private Camera _camera;
  [SerializeField]
  private GameObject _handPrefab;
  [SerializeField]
  private float _handSpeed;

  private Transform _handTransform;
  private bool _isInit;
  private float _lastTouchTime;

  private bool _isAnnouncementPlayed;
  private bool _isHintShown;

  private Shape _shape;
  private List<Vector3> _movementPositions;
  private int _movementPointIndex;

  private float _relativeHandSpeed;

  public void Init(Shape shape)
  {
    _shape = shape;
    _lastTouchTime = Time.time;
    _handTransform = Instantiate(_handPrefab).transform;
    _isInit = true;
  }
  private void Update()
  {
    if (!_isInit)
    {
      return;
    }

    CheckInput();
    
    CheckTime();

    if (_isHintShown)
    {
      
      Vector3 target = _movementPositions[_movementPointIndex];
      _handTransform.position = Vector3.MoveTowards(_handTransform.position, target, _relativeHandSpeed * Time.deltaTime);
      float distance = (target - _handTransform.position).sqrMagnitude;
      if (distance < .5f)
      {
        _movementPointIndex++;
      }

      if (_movementPointIndex >= _movementPositions.Count)
      {
        _handTransform.position = _movementPositions[0];
        _movementPointIndex = 0;
      }
    }
  }

  private void CheckInput()
  {
    if (Input.touchCount != 0)
    {
      Vector3 touchPosition = _camera.ScreenToWorldPoint(Input.GetTouch(0).position);
      RaycastHit2D hit = Physics2D.Raycast(touchPosition, Vector2.zero);

      if (hit.collider != null)
      {
        _lastTouchTime = Time.time;
        ResetHints();
      }
    } 
  }

  private void ResetHints()
  {
    _isAnnouncementPlayed = false;
    _isHintShown = false;
    _handTransform.gameObject.SetActive(false);  
  }
  
  private void CheckTime()
  {
    float timeDifference = Time.time - _lastTouchTime;

    if (timeDifference > 7 && !_isAnnouncementPlayed)
    {
      _isAnnouncementPlayed = true;
      _announcer.PlayAnnouncement();
    }
    else if (timeDifference > 14 && !_isHintShown)
    {
      _movementPositions = _shape.GetLineRendererPoints();
      _handTransform.position = _movementPositions[0];
      _movementPointIndex = 0;

      _relativeHandSpeed = Vector3.Distance(_movementPositions[0], _movementPositions[^1]) * _handSpeed;
      _handTransform.gameObject.SetActive(true);
      _isHintShown = true;
    }
  }
}