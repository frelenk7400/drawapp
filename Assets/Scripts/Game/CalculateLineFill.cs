﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace DefaultNamespace.Game
{
  public class CalculateLineFill : MonoBehaviour
  {
    [SerializeField]
    private Camera _camera;

    private Shape _currentShape;
    private bool _isLineFollows;
    private float _lineFillAmount = 1;
    
    private float _targetQuarter = 180;
    
    private float _pointsAspect = 1;
    private List<Vector3> _lineRendererPoints;

    private bool _isCalculationDone;
    private Transform _transformFollowLine;

    private bool _isInit;
    public void Init (Shape shape, Transform transformFollowLine)
    {
      _transformFollowLine = transformFollowLine;
      _currentShape = shape;
      _currentShape.OnLineClose += OnLineClose;
      _currentShape.OnShapeClose += OnShapeClose;

      CalculateFollowLineAspect();
      
      _transformFollowLine.position = _lineRendererPoints[0];

      _isInit = true;
    }

    private void OnShapeClose()
    {
      _isCalculationDone = true;
      _transformFollowLine.position = _lineRendererPoints[^1];
    }

    private void OnLineClose()
    {
      CalculateFollowLineAspect();
      ResetFill();
    }

    private void Update()
    {
      if (!_isInit)
      {
        return;
      }
      
      if (Input.touchCount != 0 && !_isCalculationDone)
      {
        Vector3 touchPosition = _camera.ScreenToWorldPoint(Input.GetTouch(0).position);

        if (_isLineFollows)
        {
          TraceType traceType = _currentShape.TraceType;

          if (traceType is TraceType.Vertical or TraceType.Horizontal)
          {
            LinearFill(touchPosition);
          } 
          else
          {
            RadialFill(touchPosition); 
          }
          
          MoveSpriteAlongLine();
          
          _currentShape.SetFillValue(_lineFillAmount);
          return;
        }

        RaycastHit2D hit = Physics2D.Raycast(touchPosition, Vector2.zero);

        if (hit.collider != null)
        {
          _isLineFollows = true;
        }
      } 
      else if (!_isCalculationDone)
      {
        ResetFill();
      }
    }

    private void LinearFill (Vector3 touchPosition)
    {
      Transform lineRendererTransform = _currentShape.LineRendererTransform;
      Vector3 rotation = lineRendererTransform.eulerAngles;
      rotation.z -= _currentShape.OffsetZRotation;

      List<Vector3> lineRendererStartEndPositions = _currentShape.GetLineRendererStartEndPositions();

      Vector3 firstPosition = Vector3.zero;
      Vector3 secondPosition = Vector3.zero;

      float lineWidth = Mathf.Abs(lineRendererStartEndPositions[0].x) + Mathf.Abs(lineRendererStartEndPositions[1].x);
      float lineHeight = Mathf.Abs(lineRendererStartEndPositions[0].y) + Mathf.Abs(lineRendererStartEndPositions[1].y);

      TraceType traceShape = _currentShape.TraceType;

      if (traceShape == TraceType.Horizontal)
      {
        firstPosition.x = lineRendererTransform.position.x - Mathf.Sin(rotation.z * Mathf.Deg2Rad) * lineWidth / 2.0f;
        firstPosition.y = lineRendererTransform.position.y - Mathf.Cos(rotation.z * Mathf.Deg2Rad) * lineWidth / 2.0f;

        secondPosition.x = lineRendererTransform.position.x + Mathf.Sin(rotation.z * Mathf.Deg2Rad) * lineWidth / 2.0f;
        secondPosition.y = lineRendererTransform.position.y + Mathf.Cos(rotation.z * Mathf.Deg2Rad) * lineWidth / 2.0f;
      } 
      else
      {
        firstPosition.x = lineRendererTransform.position.x - Mathf.Cos(rotation.z * Mathf.Deg2Rad) * lineHeight / 2.0f;
        firstPosition.y = lineRendererTransform.position.y - Mathf.Sin(rotation.z * Mathf.Deg2Rad) * lineHeight / 2.0f;

        secondPosition.x = lineRendererTransform.position.x + Mathf.Cos(rotation.z * Mathf.Deg2Rad) * lineHeight / 2.0f;
        secondPosition.y = lineRendererTransform.position.y + Mathf.Sin(rotation.z * Mathf.Deg2Rad) * lineHeight / 2.0f;
      }

      firstPosition.z = lineRendererTransform.position.z;
      secondPosition.z = lineRendererTransform.position.z;

      touchPosition.x = Mathf.Clamp(touchPosition.x, Mathf.Min(firstPosition.x, secondPosition.x), Mathf.Max(firstPosition.x, secondPosition.x));
      touchPosition.y = Mathf.Clamp(touchPosition.y, Mathf.Min(firstPosition.y, secondPosition.y), Mathf.Max(firstPosition.y, secondPosition.y));

      if (traceShape == TraceType.Vertical)
      {
        _lineFillAmount = 1 - Mathf.Abs(Vector2.Distance(touchPosition, firstPosition) / Vector2.Distance(firstPosition, secondPosition));
      } 
      else
      {
        _lineFillAmount = Mathf.Abs(Vector2.Distance(touchPosition, firstPosition) / Vector2.Distance(firstPosition, secondPosition));
      }
    }
    
    private void RadialFill(Vector3 touchPosition)
    {
      Vector3 direction = touchPosition - _currentShape.GetShapeCenter();
      float angleOffset = -90;
      float angle = Mathf.Atan2(-direction.x, -direction.y) * Mathf.Rad2Deg + angleOffset;

      if (angle < 0)
      {
        angle += 360;
      }

      angle = Mathf.Clamp(angle, 0, 360);

      if (!(angle >= 0 && angle <= _targetQuarter))
      {
        _lineFillAmount = 1;
        return;
      }

      if (angle >= _targetQuarter / 2)
      {
        _targetQuarter += 90;
      } 
      else if (angle < 45)
      {
        _targetQuarter = 90;
      }

      _targetQuarter = Mathf.Clamp(_targetQuarter, 90, 360);

      _lineFillAmount = 1 - Mathf.Abs(angle / 360.0f);
    }

    private void MoveSpriteAlongLine()
    {
      int pointIndex = (int)((1 - _lineFillAmount) / _pointsAspect);
      float lerp = (1 - _lineFillAmount) / _pointsAspect - pointIndex;
      Vector3 temp = Vector3.Lerp(_lineRendererPoints[pointIndex], _lineRendererPoints[pointIndex + 1], lerp);

      _transformFollowLine.position = temp;
    }
    private void ResetFill()
    {
      _transformFollowLine.position = _currentShape.GetLineRendererStartEndPositions()[0];
      _lineFillAmount = 1;
      _isLineFollows = false;
      _currentShape.SetFillValue(_lineFillAmount);
    }

    private void CalculateFollowLineAspect()
    {
      _lineRendererPoints = _currentShape.GetLineRendererPoints();
      _pointsAspect = 1 / ((float)_lineRendererPoints.Count - 1);
    }

    private void OnDestroy()
    {
      if (_currentShape != null)
      {
        _currentShape.OnLineClose -= OnLineClose;
        _currentShape.OnShapeClose -= OnShapeClose;
      }
    }
  }
}