using System;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
public class NestedScrollRect : ScrollRect
{
  [SerializeField]
  protected ScrollRect _parentScrollRect;

  private bool _routeToParent;

  private IInitializePotentialDragHandler [] _parentInitializePotentialDragHandlers;
  private IBeginDragHandler [] _parentBeginDragHandlers;
  private IDragHandler [] _parentDragHandlers;
  private IEndDragHandler [] _parentEndDragHandlers;

  public void InitScroll()
  {
    _parentInitializePotentialDragHandlers = _parentScrollRect.GetComponents<IInitializePotentialDragHandler>();
    _parentBeginDragHandlers = _parentScrollRect.GetComponents<IBeginDragHandler>();
    _parentDragHandlers = _parentScrollRect.GetComponents<IDragHandler>();
    _parentEndDragHandlers = _parentScrollRect.GetComponents<IEndDragHandler>();
  }
  protected override void Awake()
  {
    if (_parentScrollRect == null)
    {
      return;
    }

    InitScroll();
  }

  public override void OnInitializePotentialDrag (PointerEventData eventData)
  {
    for (int i = 0; i < _parentInitializePotentialDragHandlers.Length; ++i)
    {
      _parentInitializePotentialDragHandlers[i].OnInitializePotentialDrag(eventData);
    }

    base.OnInitializePotentialDrag(eventData);
  }

  public override void OnBeginDrag (PointerEventData eventData)
  {
    _routeToParent = (!horizontal && Mathf.Abs(eventData.delta.x) > Mathf.Abs(eventData.delta.y)) || (!vertical && Mathf.Abs(eventData.delta.x) < Mathf.Abs(eventData.delta.y));

    if (_routeToParent)
    {
      for (int i = 0; i < _parentBeginDragHandlers.Length; ++i)
      {
        _parentBeginDragHandlers[i].OnBeginDrag(eventData);
      }
    } 
    else
    {
      base.OnBeginDrag(eventData);
    }
  }

  public override void OnDrag (PointerEventData eventData)
  {
    if (_routeToParent)
    {
      for (int i = 0; i < _parentDragHandlers.Length; ++i)
      {
        _parentDragHandlers[i].OnDrag(eventData);
      }
    }
    else
    {
      base.OnDrag(eventData);
    }
  }

  public override void OnEndDrag (PointerEventData eventData)
  {
    if (_routeToParent)
    {
      for (int i = 0; i < _parentEndDragHandlers.Length; ++i)
      {
        _parentEndDragHandlers[i].OnEndDrag(eventData);
      }
    } 
    else
    {
      base.OnEndDrag(eventData);
    }

    _routeToParent = false;
  }
}