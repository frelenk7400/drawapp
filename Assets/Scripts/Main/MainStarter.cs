using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Serialization;
using UnityEngine.UI;

public class MainStarter : MonoBehaviour
{
  [SerializeField]
  private MainSceneSettings _mainSceneSettings;
  [SerializeField]
  private ScrollRect _verticalCategoryScroll;
  [FormerlySerializedAs("_horizontalCategoryScroll"),SerializeField]
  private HorizontalCategoryScroll _horizontalCategoryScrollPrefab;
  [SerializeField]
  private Transform _canvasTransform;

  private DataReadWrite _dataReadWrite;
  
  public void Start()
  {
    _dataReadWrite = new DataReadWrite();
    
    ScrollRect verticalCategoryScroll = Instantiate(_verticalCategoryScroll, _canvasTransform);

    foreach (var horizontalElement in _mainSceneSettings.HorizontalScrollElement)
    {
      string character = horizontalElement.Key;
      HorizontalCategoryScroll horizontalCategoryScroll = Instantiate(_horizontalCategoryScrollPrefab, verticalCategoryScroll.content);
      horizontalCategoryScroll.Init(verticalCategoryScroll, horizontalElement.Value.CategoryName);

      for (int i = 0; i < horizontalElement.Value.Colors.Count; i++)
      {
        CharacterHolder characterHolder = Instantiate(horizontalElement.Value.Prefab, horizontalCategoryScroll.content);
        int colorIndex = i;
        characterHolder.Init(horizontalElement.Value.Character, horizontalElement.Value.Colors[i],
          () => { LoadGameScene(character, colorIndex); });
      }
    }
  }

  private void LoadGameScene(string character, int colorIndex)
  {
    DataReadWrite.CharacterDataInfo characterDataInfo = new DataReadWrite.CharacterDataInfo(colorIndex, character);
    _dataReadWrite.WriteCharacterData(characterDataInfo);
    SceneManager.LoadScene("Game");
  }
}
