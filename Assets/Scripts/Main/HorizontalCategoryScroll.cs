using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class HorizontalCategoryScroll : NestedScrollRect
{
  [SerializeField]
  private TextMeshProUGUI _category;

  public void Init (ScrollRect parentScrollRect, string categoryText)
  {
    float parentWidth = ((RectTransform)transform.parent).rect.width;
    RectTransform rectTransform = (RectTransform)transform;
    Vector2 panelSize = rectTransform.rect.size;
    panelSize.x = parentWidth;
    rectTransform.sizeDelta = panelSize;
    
    _category.text = categoryText;
    _parentScrollRect = parentScrollRect;
    InitScroll();
  }
}
