using System;
using UnityEngine;
using UnityEngine.UI;

public class CharacterHolder : MonoBehaviour
{
  [SerializeField]
  private Button _button;
  [SerializeField]
  private Image _character;

  private Action _onButtonClick;

  private void Start()
  {
    _button.onClick.AddListener(ExecuteActions);
  }

  public void Init (Sprite sprite, Color characterColor, Action onButtonClick)
  {
    _character.sprite = sprite;
    _character.color = characterColor;
    _onButtonClick = onButtonClick;
  }

  private void ExecuteActions()
  {
    _onButtonClick?.Invoke();
  }
  
  public void OnDestroy()
  {
    _button.onClick.RemoveListener(ExecuteActions);
  }
}
