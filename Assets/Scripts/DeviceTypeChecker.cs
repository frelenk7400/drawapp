using UnityEngine;
public static class DeviceTypeChecker
{
  public static bool IsTablet()
  {
    bool isTablet = DeviceDiagonalSizeInInches() > 7.5f;
    return isTablet;
  }

  private static float DeviceDiagonalSizeInInches()
  {
    float screenWidth = Screen.width / Screen.dpi;
    float screenHeight = Screen.height / Screen.dpi;
    float diagonalInches = Mathf.Sqrt(Mathf.Pow(screenWidth, 2) + Mathf.Pow(screenHeight, 2));
    return diagonalInches;
  }
}