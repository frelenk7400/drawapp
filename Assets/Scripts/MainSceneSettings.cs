using System;
using System.Collections.Generic;
using AYellowpaper.SerializedCollections;
using UnityEngine;

[CreateAssetMenu(fileName = "MainSettings", menuName = "", order = 1)]
public class MainSceneSettings : ScriptableObject
{
  [SerializedDictionary("Character", "Description")]
  public SerializedDictionary<string, ElementSettings> HorizontalScrollElement;
  public GameObject FollowLinePathObject;
  
  [Serializable]
  public class ElementSettings
  {
    public string CategoryName;
    public ShapeType ShapeType;
    public Sprite Character;
    public CharacterHolder Prefab;
    public Shape ShapePrefab;
    public List<Color> Colors;
  }
}

public enum ShapeType
{
  Character,
  Number,
  Figure
}